# Usage

`!<dice>d<sides>[<+|-><dice modifier>][<+|-><total modifier>]`

## Examples

```
!d20

1d20+0                 Total+0
14                     14
```

```
!2d20

2d20+0                 Total+0
8, 5                   13
```

```
!2d20+5

2d20+5                 Total+0
25, 21                 46
```

```
!2d20+0+5

2d20+0                 Total+5
7, 12                  24
```

