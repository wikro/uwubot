const config = require("./config.json");
const { Client, MessageEmbed } = require("discord.js");

const client = new Client();

const random = (max) => {
  return Math.floor(Math.random() * Math.floor(max));
};

const commandHelp = (message) => {
  const re = /^\!uwu$/i;

  if (!re.test(message.content)) {
    return false;
  }

  const text_help = [];
  text_help.push("Of c-course I'll h-help! ~*senpai finally notices me!*~");

  text_help.push("\nTo r-roll, follow this syntax:");
  text_help.push("`!<dice>d<sides>[<+|-><dice modifier>][<+|-><total modifier>]`");

  text_help.push("\nFor example:");
  text_help.push("`!d20` will roll a single d20+0");
  text_help.push("`!2d20` will roll two d20+0");
  text_help.push("`!2d20+5` will roll two d20+5");
  text_help.push("`!2d20+0+5` will roll two d20+0 and add 5 to the total result of all rolls");

  text_help.push("\nMax values: `10d100[+|-]1000[+|-]1000`");

  text_help.push("\nYou can also perform c-checks, initiative and to-hit easily, u-using this syntax:");
  text_help.push("`<!|+|-><ini|hit|str|int|dex|wis|con|cha>[<+|-><modifier>]`");

  text_help.push("\nFor example:");
  text_help.push("`!ini+4` will roll a single d20+4 for initiative");
  text_help.push("`+dex+10` will roll two d20+10 for dexterity, and highlight the highest number (advantage)");
  text_help.push("`-str` will roll two d20+0 for strength, and highlight the lowest number (disadvantage)");

  text_help.push("\nMax values: `[+|-]1000`");

  text_help.push("\nTo f-flip coins, follow this syntax:");
  text_help.push("`!flip[coins]`");

  text_help.push("\nFor example:");
  text_help.push("`!flip` will flip a single coin");
  text_help.push("`!flip5` will flip 5 coins");

  text_help.push("\nMax values: `10`");

  return text_help.join("\n");
};

const commandFlip = (message) => {
  const re = /^\!(flip)(\d+)?$/i;

  if (!re.test(message.content)) {
    return false;
  }

  let [full, command, coins] = message.content.match(re);
  coins = Number(coins) || 1;

  if (coins > 10) {
    return "T-that's an awful l-lot of coin flips... P-please flip at most `10`, senpai...";
  }

  const embed = new MessageEmbed();
  const flips = [];

  for (let c = 0; c < coins; c++) {
    flips.push(random(2) > 0 ? "heads" : "tails");
  }

  embed.addFields([{
    name: "Coin flips",
    value: `${flips.slice(0, 5).join(", ")}${flips.length > 5 ? ",\n" + flips.slice(5).join(", ") : ""}`,
    inline: true
  },
  {
    name: "Heads",
    value: flips.filter(c => c === "heads").length,
    inline: true
  },
  {
    name: "Tails",
    value: flips.filter(c => c === "tails").length,
    inline: true
  }]);

  return embed;
};

const commandCheck = (message) => {
  const re = /^([\!\+\-])(ini|hit|str|int|dex|wis|con|cha)([\+\-]\d+)?$/i;
  const trim = message.content.replace(/ /gi, "").toLowerCase();

  if (!re.test(trim)) {
    return false;
  }

  let [full, advantage, check, check_mod] = trim.match(re);
  check_mod = check_mod || "+0";

  if (Math.abs(eval(check_mod)) > 1000) {
    return "Kya! P-please, use at most `[+|-]1000`, senpai.";
  }

  switch(check) {
    case 'hit':
      check = "To hit";
      break;
    case 'str':
      check = "Strength";
      break;
    case 'int':
      check = "Intelligence";
      break;
    case 'dex':
      check = "Dexterity";
      break;
    case 'wis':
      check = "Wisdom";
      break;
    case 'con':
      check = "Constitution";
      break;
    case 'cha':
      check = "Charisma";
      break;
    default:
      check = "Initiative";
  }

  const embed = new MessageEmbed();
  const rolls = [];
  const dice = advantage === "!" ? 1 : 2;

  for (let d = 0; d < dice; d++) {
    rolls.push(eval(`${random(20) + 1}${check_mod}`));
  }

  embed.addFields([{
    name: "Type",
    value: check,
    inline: true
  },
  {
    name: `${dice}d20${check_mod}`,
    value: rolls.join(", "),
    inline: true
  }]);

  if (advantage === "+") {
    embed.addFields([{
      name: "Advantage",
      value: `**${Math.max(...rolls)}**`,
      inline: true
    }]);
  } else if (advantage === "-") {
    embed.addFields([{
      name: "Disadvantage",
      value: `**${Math.min(...rolls)}**`,
      inline: true
    }]);
  }

  return embed;
};

const commandRoll = (message) => {
  const re = /^\!(\d+)?d(\d+)([\+\-]\d+)?([\+\-]\d+)?$/i;
  const split = message.content.replace(/ /gi, "").split(",");

  if (!split.every(command => re.test(command))) {
    return false;
  }

  if (split.length > 5) {
    return "N-no way I can do that many, senpai! The most I can do is 5 roll commands at a time!";
  }

  const embed = new MessageEmbed();
  const totals = [];

  for (const command of split) {
    let [full, dice, sides, dice_mod, total_mod] = command.match(re);
  
    dice = Number(dice) || 1;
    sides = Number(sides) || 1;
    dice_mod = dice_mod || "+0";
    total_mod = total_mod || "+0";
  
    if (dice > 10 || sides > 100 || Math.abs(eval(dice_mod)) > 1000 || Math.abs(eval(total_mod)) > 1000) {
      return "B-baka! These are the max values, senpai: `10d100[+|-]1000[+|-]1000`";
    }
  
    const rolls = [];
  
    for (let d = 0; d < dice; d++) {
      rolls.push(eval(`${random(sides) + 1}${dice_mod}`));
    }
  
    const total = eval(`${rolls.join("+")}${total_mod}`);
    totals.push(total);
  
    embed.addFields([{
      name: `Command`,
      value: command,
      inline: true
    },
    {
      name: `${dice}d${sides}${dice_mod}`,
      value: rolls.join(", "),
      inline: true
    },
    {
      name: `Total${total_mod}`,
      value: `**${total}**`,
      inline: true
    }]);
  }

  if (totals.length > 1) {
    const grand_total = `${totals.join("+")}`;

    embed.addFields([{
      name: "Grand total",
      value: `${grand_total} = **${eval(grand_total)}**`,
      inline: false
    }]);
  }

  return embed;
};

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}`);
});

client.on("message", (message) => {
  const roll = commandRoll(message);
  if (roll) {
    message.reply(roll);
  }

  const check = commandCheck(message);
  if (check) {
    message.reply(check);
  }

  const flip = commandFlip(message);
  if (flip) {
    message.reply(flip);
  }

  const help = commandHelp(message);
  if (help) {
    message.author.send(help);
  }

  return false;
});

client.login(config.secret);

